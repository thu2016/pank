var links = document.getElementsByTagName("a");

for(var i=0; i<links.length; i++){
    links[i].setAttribute("data-href", links[i].getAttribute("href"));
    links[i].removeAttribute("href");
    links[i].onclick = function(){
        window.location = this.getAttribute("data-href");
    };
}